## Тестовое задание для frontend разработчика ##
1. Сделать адаптивную верстку одной страницы по [макетам](https://www.figma.com/file/3bCNBAXduB1x8qsEcJkVXt/frontend)
2. Реализовать функционал калькулятора

#### Описание калькулятора ####
Specify your project details - тип проекта, не виляет на отображение калькулятора, по умолчанию выбрано Still, значения 
radio кнопок animation, static

Number of frames - обязательное поле
Average render time per frame on your PC - два поля определяющие время, должно быть заполнено хотя бы одно
Your PС - представляет собой наборы видеокарт и их benchmark'и, выпадающие списки связанны между собой. 
Выпадающий список не может содержать пустое значение. Количество видеокарт список со значениями от 1 до 10.

Add another type card - добавляет видеокарту, можно добавлять любое количество видеокарт. 
Видеокарты можно удалять нажав на "-". В форме всегда должна быть хотя бы одна видеокарта.

Список видеокарт и их benchmark'и находятся в файле gpu.json.

Кнопка Calculate рассчитывает стоимость и время
 
Render time рассчитывается по формуле clientBenchmark / 50 * clientRenderTime * framesCount
Price рассчитывается по формуле Render time * 10 

* clientBenchmark - сумма benchmark'ов указанных видеокарт
* clientRenderTime - Average render time per frame on your PC (время в минутах)
* framesCount - Number of frames (количество кадров)

После расчета в консоль вывести объект
``` 
{
    models: строка, список моделей видокарт, через точку с запятой 
    benchmark: целое число, общий benchmark по всем видеокартам
    rendererInfo: {
        sceneType: строка static или animation,
        framesCount: целое число, количество кадров
        renderTime: целое число, время рендера в минутах
    }
}
```

Реализовать кальулятор необходимо на VueJS
